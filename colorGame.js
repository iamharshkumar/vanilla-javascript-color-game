var numSquares = 6;
var colors = randomColorGenerate(6);
var squares = document.querySelectorAll(".square");
var pickedColor = pickColor();
var colorDisplay = document.getElementById("colorDisplay");
var messageDisplay = document.querySelector("#message");
var h1 =document.querySelector("h1");
var resetButton =document.querySelector("#reset");
var easyBtn =document.querySelector("#easyBtn");
var hardBtn =document.querySelector("#hardBtn");

easyBtn.addEventListener("click",function(){
	hardBtn.classList.remove("selected");
	easyBtn.classList.add("selected");
	numSquares = 3;
	colors = randomColorGenerate(numSquares);
	messageDisplay.textContent = "";
	pickedColor = pickColor();
	colorDisplay.textContent = pickedColor;
	for(var i = 0;i < squares.length;i++)
	if(colors[i]){
		squares[i].style.background = colors[i];
	}else{
		squares[i].style.background = "None";
	}
	
});

hardBtn.addEventListener("click",function(){
	hardBtn.classList.add("selected");
	easyBtn.classList.remove("selected");
	numSquares = 6;
	colors = randomColorGenerate(numSquares);
	messageDisplay.textContent = "";
	pickedColor = pickColor();
	colorDisplay.textContent = pickedColor;
	for(var i = 0;i < squares.length;i++)
	
		squares[i].style.background = colors[i];
	
		squares[i].style.background = "block";
	
});


resetButton.addEventListener("click", function(){
	

	colors = randomColorGenerate(numSquares);

	pickedColor = pickColor();
	colorDisplay.textContent = pickedColor;
	this.textContent = "New colors";
	messageDisplay.textContent = "";
	for(var i=0;i<squares.length;i++){
		squares[i].style.background = colors[i];
		h1.style.background = "steeleblue";
	}
	h1.style.background = "steeleblue";
});

colorDisplay.textContent = pickedColor;
for(var i=0; i<squares.length; i++)
{
	squares[i].style.background = colors[i];

	squares[i].addEventListener("click", function(){
		var clickedColor = this.style.background;
		console.log(clickedColor,pickedColor);
		if(clickedColor === pickedColor){
		   messageDisplay.textContent = "Correct!!";
		   resetButton.textContent = "Try Again?"
		   changeColor(pickedColor);
		   h1.style.background = clickedColor;
		} else {
			this.style.background = "#232323";
			messageDisplay.textContent = "Wrong!!";
		}
	});
}
 function changeColor(color){
	for(var i=0;i<squares.length;i++){
		squares[i].style.background = color;
	}
}
function pickColor(){
	var random = Math.floor(Math.random() * colors.length);
	return colors[random];
}
function randomColorGenerate(num){
	var arr = [];

	for(i=0;i<num;i++){
		arr.push(randomColor())
	}

	return arr;
}

function randomColor(){
	var r =Math.floor(Math.random() * 256);
	var b =Math.floor(Math.random() * 256);
	var g =Math.floor(Math.random() * 256);
	"rgb(r,g,b)"
	return "rgb(" + r + ", "  +  g + ", "  +  b + ")";
}